<?php

/**
 * @file
 * Hooks provided by the EnhancedUser module.
 */

use Drupal\Core\Cache\CacheableMetadata;

/**
 * Add additional data to the user-info rest response.
 *
 * @param array $additional_data
 *   Data to add to the user-info rest response.
 * @param \Drupal\Core\Cache\CacheableMetadata $cache_metadata
 *   Cache metadata to add to the user-info rest response.
 */
function hook_enhanced_user_user_info(array &$additional_data, CacheableMetadata &$cache_metadata) {
  $node = \Drupal\node\Entity\Node::load(1);
  $additional_data['something'] = ['some' => 'data'];
  $cache_metadata->addCacheableDependency($node);
}
