<?php

namespace Drupal\enhanced_user;

/**
 * Interface for "enhanced_user.user_creator" service.
 */
interface UserCreatorInterface {

  /**
   * Method to create a drupal user.
   */
  public function createUser($name, $email = NULL, array $roles = []);

}
