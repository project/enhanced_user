<?php

namespace Drupal\enhanced_user\Plugin\rest\resource;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\user\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides a resource to get user information.
 *
 * @RestResource(
 *   id = "enhanced_user_user_info",
 *   label = @Translation("Enhanced User info"),
 *   uri_paths = {
 *     "canonical" = "/api/rest/enhanced-user/user-info"
 *   }
 * )
 */
class UserInfo extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The "module_handler" service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new UserInfoResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The "module_handler" service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('enhanced_user'),
      $container->get('current_user'),
      $container->get('module_handler')
    );
  }

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get(): ResourceResponse {
    $user = User::load($this->currentUser->id());
    $data = [
      'base' => $user,
      'uid' => $user->id(),
      'uuid' => $user->uuid(),
      'roles' => $user->getRoles(),
    ];

    // @todo fix user_api_user_info hook implements in other modules to enhanced_user_user_info.
    $additional_data = [];
    // Pass the $cache_metadata object as the context parameter,
    // therefore hook implements can add cacheable dependencies to it,
    // and finally merge to the response object.
    $cache_metadata = new CacheableMetadata();
    $this->moduleHandler->alter('enhanced_user_user_info', $additional_data, $cache_metadata);

    $response = new ResourceResponse($data + $additional_data, 200);
    $cache_metadata->addCacheableDependency($user);
    $cache_metadata->addCacheContexts(['user']);
    $response->addCacheableDependency($cache_metadata);
    return $response;
  }

}
