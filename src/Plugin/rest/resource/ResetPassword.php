<?php

namespace Drupal\enhanced_user\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\UserAuthInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Provides a resource to reset user password.
 *
 * @todo Support SMS code for authentication.
 *
 * @RestResource(
 *   id = "enhanced_user_reset_password",
 *   label = @Translation("Enhanced user Reset password"),
 *   uri_paths = {
 *     "create" = "/api/rest/enhanced-user/reset-password"
 *   }
 * )
 */
class ResetPassword extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The "user.auth" service.
   *
   * @var \Drupal\user\UserAuthInterface
   */
  protected $userAuth;

  /**
   * Constructs a new ResetPassword object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Drupal\user\UserAuthInterface $user_auth
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    UserAuthInterface $user_auth) {

    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
    $this->userAuth = $user_auth;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('enhanced_user'),
      $container->get('current_user'),
      $container->get('user.auth')
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param array $data
   *   Data posted from http client.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(array $data): ModifiedResourceResponse {
    if (isset($data['username'])) {
      $username = $data['username'];
    }
    elseif ($this->currentUser->isAuthenticated()) {
      $username = $this->currentUser->getAccountName();
    }
    else {
      throw new BadRequestHttpException('username is required.');
    }

    if (!isset($data['password']) || !isset($data['new_password'])) {
      throw new BadRequestHttpException('password and new_password is required.');
    }

    // @todo Use authenticateWithFloodProtection when #2825084 lands.
    $uid = $this->userAuth->authenticate($username, $data['password']);
    if ($uid) {
      if ($this->currentUser->isAuthenticated() && $this->currentUser->id() !== $uid) {
        throw new BadRequestHttpException('Can only change your own password!');
      }
    }
    else {
      throw new BadRequestHttpException('username or password was wrong.');
    }

    /** @var \Drupal\user\UserInterface $user */
    $user = $this->currentUser->getAccount();
    $user
      ->setPassword($data['new_password'])
      ->save();

    return new ModifiedResourceResponse($user, 200);
  }

}
