<?php

namespace Drupal\enhanced_user\Plugin\rest\resource;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides a resource to upload user avatar.
 *
 * @RestResource(
 *   id = "enhanced_user_upload_avatar",
 *   label = @Translation("Enhanced user upload avatar"),
 *   uri_paths = {
 *     "create" = "/api/rest/enhanced-user/upload-avatar"
 *   }
 * )
 */
class UploadAvatar extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The "file_system" service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The "config.factory" service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The "uuid" service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * Constructs a new UploadAvatar object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The "file_system" service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The "config.factory" service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The "uuid" service.
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    FileSystemInterface $fileSystem,
    ConfigFactoryInterface $configFactory,
    UuidInterface $uuid) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->fileSystem = $fileSystem;
    $this->configFactory = $configFactory;
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('enhanced_user'),
      $container->get('current_user'),
      $container->get('file_system'),
      $container->get('config.factory'),
      $container->get('uuid')
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param array $data
   *   Data posted from client.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(array $data): ModifiedResourceResponse {

    /** @var \Drupal\user\UserInterface $user */
    $user = $this->currentUser->getAccount();

    if ($data['base64']) {
      $file_data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data['base64']));
      if ($file_data == FALSE) {
        throw new BadRequestHttpException('Base64 data transfer fails!');
      }

      // Determine image type.
      $f = finfo_open();
      $mimeType = finfo_buffer($f, $file_data, FILEINFO_MIME_TYPE);
      $ext = $this->getMimeTypeExtension($mimeType);
      // Generate fileName.
      $name = $this->uuid->generate() . $ext;

      // Save to disk.
      $default_scheme = $this->configFactory->get('system.file')->get('default_scheme');
      $directory = $default_scheme . '://pictures/rest';
      $this->fileSystem->prepareDirectory($directory, FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::CREATE_DIRECTORY);
      $file = file_save_data($file_data, $directory . $name);

      // If user entity type has the user_picture field, save this file to it.
      if ($user->hasField('user_picture')) {
        $user->set('user_picture', $file)
          ->save();
      }
    }

    return new ModifiedResourceResponse($user, 200);
  }

  /**
   * Get extend name by mime type.
   */
  public static function getMimeTypeExtension($mimeType): string {
    $mimeTypes = [
      'image/png' => 'png',
      'image/jpeg' => 'jpg',
      'image/gif' => 'gif',
      'image/bmp' => 'bmp',
      'image/svg+xml' => 'svg',
    ];
    if (!isset($mimeTypes[$mimeType])) {
      throw new BadRequestHttpException('Only support one of png/jpg/gif/bmp/svg format.');
    }
    return '.' . $mimeTypes[$mimeType];
  }

}
