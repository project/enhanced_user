<?php

namespace Drupal\enhanced_user\Plugin\rest\resource;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to alter user profile information.
 *
 * @RestResource(
 *   id = "enhanced_user_user_profile",
 *   label = @Translation("Enhanced user profile"),
 *   uri_paths = {
 *     "canonical" = "/api/rest/enhanced-user/user-profile"
 *   }
 * )
 */
class UserProfile extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new UserProfile object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('enhanced_user'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to PATCH requests.
   *
   * @param array $data
   *   Data posted from client.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function patch(array $data): ModifiedResourceResponse {

    if (!($this->currentUser instanceof AccountInterface)) {
      throw new AccessDeniedHttpException(t('Only can change own profile.'));
    }

    $user = User::load($this->currentUser->id());

    if (isset($data['mail']) && !empty($data['mail'])) {
      $user->set('mail', $data['mail']);
    }

    if (isset($data['nickname']) && !empty($data['nickname'])) {
      $user->set('nickname', $data['nickname']);
    }

    if (isset($data['sex']) && !empty($data['sex'])) {
      $user->set('sex', $data['sex']);
    }

    if (isset($data['birthday']) && !empty($data['birthday'])) {
      $date = new DrupalDateTime(
        $data['birthday'],
        new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE)
      );
      $user->set('birthday', $date->format(DateTimeItemInterface::DATE_STORAGE_FORMAT));
    }

    $user->save();

    return new ModifiedResourceResponse($user, 200);
  }

}
