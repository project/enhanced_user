<?php

namespace Drupal\enhanced_user;

use Drupal\user\UserInterface;

/**
 * Provides a service to create drupal user.
 */
class UserCreator implements UserCreatorInterface {

  /**
   * Drupal\language\ConfigurableLanguageManagerInterface definition.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The "entity_type.manager" service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The "config.factory" service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The "password_generator" service.
   *
   * @var \Drupal\Core\Password\PasswordGeneratorInterface
   */
  protected $passwordGenerator;

  /**
   * Constructs a new UserCreator object.
   */
  public function __construct(
    $language_manager,
    $logger_factory,
    $entityTypeManager,
    $configFactory,
    $password_generator
  ) {
    $this->languageManager = $language_manager;
    $this->loggerFactory = $logger_factory;
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $configFactory;
    $this->passwordGenerator = $password_generator;
  }

  /**
   * Create a new user account.
   *
   * @param string $name
   *   User's name on Provider.
   * @param string $email
   *   User's email address.
   * @param array $roles
   *   Roles attach to the user.
   *
   * @return \Drupal\user\UserInterface
   *   User entity if user was created
   *   False otherwise
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function createUser($name, $email = NULL, array $roles = []) {

    // Check if site configuration allows new users to register.
    if ($this->isRegistrationDisabled()) {
      throw new \Exception('Register new users is not allow.');
    }

    // Get the current UI language.
    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    // Initializes the user fields.
    $fields = $this->getUserFields($name, $email, $langcode);

    // Create new user account.
    /** @var \Drupal\user\Entity\User $new_user */
    $new_user = $this->entityTypeManager->getStorage('user')
      ->create($fields);
    foreach ($roles as $role) {
      $new_user->addRole($role);
    }

    $new_user->activate();
    $new_user->save();

    return $new_user;
  }

  /**
   * Checks if user registration is disabled.
   *
   * @return bool
   *   True if registration is disabled
   *   False if registration is not disabled
   */
  protected function isRegistrationDisabled(): bool {
    // Check if Drupal account registration settings is Administrators only.
    if ($this->configFactory->get('user.settings')->get('register') == 'admin_only') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Returns an array of fields to initialize the creation of the user.
   *
   * @param string $name
   *   User's name on Provider.
   * @param mixed $email
   *   User's email address.
   * @param string $langcode
   *   The current UI language.
   *
   * @return array
   *   Fields to initialize for the user creation.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getUserFields(string $name, $email, string $langcode): array {
    return [
      'name' => $this->generateUniqueUsername($name),
      'mail' => $email,
      'init' => $email,
      'pass' => $this->userPassword(32),
      'status' => $this->getNewUserStatus(),
      'langcode' => $langcode,
      'preferred_langcode' => $langcode,
      'preferred_admin_langcode' => $langcode,
    ];
  }

  /**
   * Ensures that Drupal usernames will be unique.
   *
   * Drupal usernames will be generated so that the user's full name on Provider
   * will become user's Drupal username. This method will check if the username
   * is already used and appends a number until it finds the first available
   * username.
   *
   * @param string $name
   *   User's full name on provider.
   *
   * @return string
   *   Unique drupal username.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function generateUniqueUsername(string $name): string {
    $name = mb_substr($name, 0, UserInterface::USERNAME_MAX_LENGTH);
    $name = str_replace(' ', '', $name);
    $name = strtolower($name);

    // Add a trailing number if needed to make username unique.
    $base = $name;
    $i = 1;
    $candidate = $base;
    while ($this->loadUserByProperty('name', $candidate)) {
      // Calculate max length for $base and truncate if needed.
      $max_length_base = UserInterface::USERNAME_MAX_LENGTH - strlen((string) $i) - 1;
      $base = mb_substr($base, 0, $max_length_base);
      $candidate = $base . $i;
      $i++;
    }

    // Trim leading and trailing whitespace.
    return trim($candidate);
  }

  /**
   * Loads existing Drupal user object by given property and value.
   *
   * Note that first matching user is returned. Email address and account name
   * are unique so there can be only zero or one matching user when
   * loading users by these properties.
   *
   * @param string $field
   *   User entity field to search from.
   * @param string $value
   *   Value to search for.
   *
   * @return \Drupal\user\UserInterface|false
   *   Drupal user account if found
   *   False otherwise
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadUserByProperty(string $field, string $value) {
    /** @var \Drupal\user\UserInterface $users */
    $users = $this->entityTypeManager
      ->getStorage('user')
      ->loadByProperties([$field => $value]);

    if (!empty($users)) {
      return current($users);
    }

    // If user was not found, return FALSE.
    return FALSE;
  }

  /**
   * Wrapper for user_password.
   *
   * We need to wrap the legacy procedural Drupal API functions so that we are
   * not using them directly in our own methods. This way we can unit test our
   * own methods.
   *
   * @param int $length
   *   Length of the password.
   *
   * @return string
   *   The password.
   *
   * @see user_password
   */
  protected function userPassword(int $length): string {
    return $this->passwordGenerator->generate($length);
  }

  /**
   * Returns the status for new users.
   *
   * @return int
   *   Value 0 means that new accounts remain blocked and require approval.
   *   Value 1 means that visitors can register new accounts without approval.
   */
  protected function getNewUserStatus(): int {
    if ($this->configFactory
      ->get('user.settings')
      ->get('register') == 'visitors') {
      return 1;
    }

    return 0;
  }

}
