# Enhanced user

Provides enhanced user features for core user module.

# Features

- Add base fields to `user` entity.
  - `nickname`
  - `sex`
  - `birthday`

- Provides 4 REST plugins for user data interaction.

  To enable these plugins, you need to enable the `rest` and `basic_auth` module which
  is built-in with drupal core.

  You may need to use the `simple_oauth` contrib module,
  if you want the oauth2 authentication instead of `cookie` or `basic_auth`.

  `restui` is a useful module which provides UIs to manage rest plugins.

  - `enhanced_user_reset_password` Alter user password.
  - `enhanced_user_upload_avatar` Upload base64 data to alter user avatar.
  - `enhanced_user_user_info` Get user information.
  - `enhanced_user_user_profile` Alter user profile information, for example: nickname\sex\birthday\email.

- Provides a service `enhanced_user.user_creator`, for creating new user with username and email.
