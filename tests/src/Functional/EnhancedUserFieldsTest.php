<?php

namespace Drupal\Tests\enhanced_user\Functional;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group enhanced_user
 */
class EnhancedUserFieldsTest extends BrowserTestBase {

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stable';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['enhanced_user'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser(
      [
        'access user profiles',
      ],
      'Kent',
      FALSE,
      [
        'nickname' => 'Edwin Kent',
        'sex' => 'female',
        'birthday' => $this->getDate()->format(DateTimeItemInterface::DATE_STORAGE_FORMAT),
      ]
    );
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the home page loads with a 200 response.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testLoad() {
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Edwin Kent');
    $this->assertSession()->pageTextContains('Female');

    $date_on_user = $this->user->birthday->date;

    /** @var \Drupal\Core\Datetime\DateFormatterInterface $date_formatter */
    $date = $this->getDate();
    // Reference to
    // "core/modules/datetime/tests/src/Functional/DateTimeFieldTest.php"
    // for date formatting.
    $date_formatter = $this->container->get('date.formatter');
    $expected = $date_formatter->format($date->getTimestamp(), 'html_date', '', DateTimeItemInterface::STORAGE_TIMEZONE);
    $expected_iso = $date_formatter->format($date->getTimestamp(), 'custom', 'Y-m-d\TH:i:s\Z', DateTimeItemInterface::STORAGE_TIMEZONE);
    $expected_markup = '<time datetime="' . $expected_iso . '">' . $expected . '</time>';
    $this->assertSession()->responseContains($expected_markup);
  }

  /**
   * Create Datetime for birthday.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   Datetime object for birthday test.
   */
  private function getDate(): DrupalDateTime {
    // Reference to
    // web/core/modules/datetime/src/DateTimeComputed.php
    // to make the render result as same as the formatter widget.
    $value = '2012-12-31';
    $date = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $value, DateTimeItemInterface::STORAGE_TIMEZONE);
    $date->setDefaultDateTime();
    return $date;
  }

}
