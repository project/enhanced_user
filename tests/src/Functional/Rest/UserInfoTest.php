<?php

namespace Drupal\Tests\enhanced_user\Functional\Rest;

use Drupal\Core\Url;
use Drupal\Tests\rest\Functional\CookieResourceTestTrait;
use Drupal\Tests\rest\Functional\ResourceTestBase;
use GuzzleHttp\RequestOptions;

/**
 * Tests getting user info via REST resource.
 *
 * @group user
 */
class UserInfoTest extends ResourceTestBase {

  use CookieResourceTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $format = 'json';

  /**
   * {@inheritdoc}
   */
  protected static $mimeType = 'application/json';

  /**
   * {@inheritdoc}
   */
  protected static $auth = 'cookie';

  /**
   * {@inheritdoc}
   */
  protected static $resourceConfigId = 'enhanced_user_user_info';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['enhanced_user'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $auth = isset(static::$auth) ? [static::$auth] : [];
    $this->provisionResource([static::$format], $auth);

    $this->setUpAuthorization('GET');
  }

  /**
   * Tests that only anonymous users can register users.
   */
  public function testGetUserInfo() {
    $user_info_url = Url::fromUri('internal:/api/rest/enhanced-user/user-info')
      ->setRouteParameter('_format', static::$format);

    $response = $this->request('GET', $user_info_url, $this->createRequestOptions());
    $headers  = $response->getHeaders();
    $this->assertResourceErrorResponse(403,
      $this->getExpectedUnauthorizedAccessMessage('GET'),
      $response,
      ['4xx-response', 'config:user.role.anonymous', 'http_response'],
      ['user.permissions'],
      'MISS');

    $response = $this->request('GET', $user_info_url, $this->createRequestOptions());
    $headers  = $response->getHeaders();
    $this->assertResourceErrorResponse(403,
      $this->getExpectedUnauthorizedAccessMessage('GET'),
      $response,
      ['4xx-response', 'config:user.role.anonymous', 'http_response'],
      ['user.permissions'],
      'HIT');

    // Verify that an authenticated user cannot register a new user, despite
    // being granted permission to do so because only anonymous users can
    // register themselves, authenticated users with the necessary permissions
    // can POST a new user to the "user" REST resource.
    $this->initAuthentication();
    $response = $this->request('GET', $user_info_url, $this->createRequestOptions());
    $headers  = $response->getHeaders();
    $body = $response->getBody()->getContents();
    $this->assertResourceResponse(200, FALSE, $response,
      [
        'config:rest.resource.enhanced_user_user_info',
        'http_response',
        'user:' . $this->account->id(),
      ],
      ['user'],
      FALSE,
      'UNCACHEABLE');

    $response = $this->request('GET', $user_info_url, $this->createRequestOptions());
    $headers  = $response->getHeaders();
    $body = $response->getBody()->getContents();
    $this->assertResourceResponse(200, FALSE, $response,
      [
        'config:rest.resource.enhanced_user_user_info',
        'http_response',
        'user:' . $this->account->id(),
      ],
      ['user'],
      FALSE,
      'UNCACHEABLE');
  }

  /**
   * Helper function to generate the request body.
   *
   * @return array
   *   Return the request options.
   */
  protected function createRequestOptions() {
    $request_options = $this->getAuthenticationRequestOptions('GET');
    $request_options[RequestOptions::HEADERS]['Content-Type'] = static::$mimeType;

    return $request_options;
  }

  /**
   * {@inheritdoc}
   */
  protected function setUpAuthorization($method) {
    switch ($method) {
      case 'GET':
        $this->grantPermissionsToAuthenticatedRole(['restful get enhanced_user_user_info']);
        break;

      default:
        throw new \UnexpectedValueException();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function assertNormalizationEdgeCases($method, Url $url, array $request_options) {}

  /**
   * {@inheritdoc}
   */
  protected function getExpectedUnauthorizedAccessCacheability() {}

}
